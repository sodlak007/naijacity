$(document).foundation()

$(document).ready(function(){
  $('.filter-section p').click(function(){
    $(this).toggleClass('active-filter');
    $(this).next().slideToggle();
  });
});

function colourFunction() {
	var myselect = document.getElementById("select1"),
	colour = myselect.options[myselect.selectedIndex].className;
	myselect.className = colour;
	myselect.blur(); //This just unselects the select list without having to click somewhere else on the page
}

var media = $('#upload');
  if (media.length) {
      var mediaDefaultValue = $('.file span.value').text();
      var mediaCharLimit = 20;

      $('.file .bt-value').click(function(){
          media.click();
      });

      media.on('change', function() {
          var value = this.value.replace("C:\\fakepath\\", "");
          var newValue;
          var valueExt;
          var charLimit;

          if (value) {
              newValue = value;
              valueExt = value.split('.').reverse()[0];
              if (newValue.length > mediaCharLimit) {
                  charLimit = mediaCharLimit - valueExt.length;

                  // truncate chars.
                  newValue = $.trim(value).substring(0, charLimit) + '…';

                  // if file name has extension, add it to newValue.
                  if (valueExt.length) {
                      newValue += valueExt;
                  }
              }
          }
          else {
        newValue = mediaDefaultValue;
      }
    $(this).parent().find('span.value').text(newValue);
  });
}